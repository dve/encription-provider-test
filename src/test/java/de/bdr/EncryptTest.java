package de.bdr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class EncryptTest {

    @Test
    public void test() {
        String result = Encrypt.encryptPassword("test", "mykey");
        Assertions.assertEquals("wLZ0PtVOdiSm5kHpX8xBVg==", result);
    }
}

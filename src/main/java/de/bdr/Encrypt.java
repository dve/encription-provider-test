package de.bdr;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.util.Base64;

public final class Encrypt {

    private Encrypt() {
    }

    public static String encryptPassword(String encString, String encKey) {
        byte[] bytes = encString.getBytes();
        byte[] encryptedBytes = cryptPassword(bytes, encKey, Cipher.ENCRYPT_MODE);
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    private static byte[] cryptPassword(byte[] password, String key, int cryptMode) {
        try {
            // MessageDigest
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] keyBytes = key.getBytes();
            byte[] keyDigest = messageDigest.digest(keyBytes);
            // Cipher
            if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
                Security.addProvider(new BouncyCastleProvider());
            }
            Provider bouncyCastleProvider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
            Cipher aes = Cipher.getInstance("AES/CBC/PKCS5Padding", bouncyCastleProvider);
            SecretKeySpec secretKeySpec = new SecretKeySpec(keyDigest, 0, 32, "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(keyDigest, 0, 16);
            aes.init(cryptMode, secretKeySpec, ivParameterSpec);
            byte[] cryptPassword = aes.doFinal(password);
            return cryptPassword;
        } catch (NoSuchPaddingException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

}

package de.bdr;

public class App {

    public static void main(String[] args) {
        System.out.println("This application encrypt the first argument with the given key as a second argument");
        System.out.println("toEncrypt: " + args[0]);
        System.out.println("key: " + args[1]);
        System.out.println("result: " + Encrypt.encryptPassword(args[0], args[1]));
    }
}
